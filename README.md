# Cinnamon

Cinnamon desktop environment build scripts for Slackware Linux.

## Info

Here you will find Slackware build scripts (SlackBuilds) for building the Cinnamon desktop environment.    

While only core and deps are required, /extra contains build scripts for more applications and their dependencies to make it a well rounded and more complete standalone desktop environment.    

## Codecs

gst-plugins-bad has many optional buildtime dependencies, see the README and consider these before building.

Also, if you are considering to rebuild Slackware's stock ffmpeg and ffmpegthumbnailer those should be done before rebuilding gst-plugins-bad.    

## Building Options

If you use [slackrepo](https://github.com/idlemoor/slackrepo) for building (recommended), you can grab a .conf and .hint files here: [slackrepo-stuff](https://gitlab.com/skaendo/slackrepo-stuff)

# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)
